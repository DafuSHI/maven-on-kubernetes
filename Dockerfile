FROM openjdk:18-jdk-alpine

ARG CI_PROJECT_URL

# hadolint ignore=DL3048
LABEL Name="burger-maker-on-kubernetes" \
      Maintainer="tbc-dev@googlegroups.com" \
      Description="burger-maker Quarkus demo application" \
      Version="1.0.0-SNAPSHOT" \
      Url="$CI_PROJECT_URL"

RUN addgroup -S cook && adduser -S cook -G cook

VOLUME /tmp

USER cook

COPY target/burger-maker-on-kubernetes-*runner.jar /app.jar

HEALTHCHECK --interval=30s --timeout=5s \
    CMD ["/wget", "-Y", "off", "-O", "-", "http://localhost/health/ready" ]

ENTRYPOINT ["java", "-jar", "/app.jar"]

EXPOSE 8080
