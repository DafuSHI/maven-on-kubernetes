package com.orange.fastronomy.maker.burger.service;


import com.orange.fastronomy.maker.burger.domain.Burger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BurgerMakerServiceTest {

    private BurgerMakerService burgerMakerService;

    @BeforeEach
    public void setUp() {
        burgerMakerService = new BurgerMakerService();
    }

    @Test
    public void testDeliver() {

        Optional<Burger> optionalBurger = burgerMakerService.deliver();

        assertTrue(optionalBurger.isPresent());
        Burger burger = optionalBurger.get();
        assertEquals("burger", burger.getName());
        assertEquals(9, burger.getIngredients().size());
        assertEquals("bun", burger.getIngredients().get(0).getName());
        assertEquals("ketchup", burger.getIngredients().get(1).getName());
        assertEquals("mayo", burger.getIngredients().get(2).getName());
        assertEquals("tomato", burger.getIngredients().get(3).getName());
        assertEquals("salad", burger.getIngredients().get(4).getName());
        assertEquals("bacon", burger.getIngredients().get(5).getName());
        assertEquals("cheddar", burger.getIngredients().get(6).getName());
        assertEquals("steak", burger.getIngredients().get(7).getName());
        assertEquals("cheddar", burger.getIngredients().get(8).getName());
    }
}
