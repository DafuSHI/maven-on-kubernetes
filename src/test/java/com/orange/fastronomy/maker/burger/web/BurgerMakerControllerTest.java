package com.orange.fastronomy.maker.burger.web;

import com.orange.fastronomy.maker.burger.domain.Burger;
import com.orange.fastronomy.maker.burger.domain.Ingredient;
import com.orange.fastronomy.maker.burger.service.BurgerMakerService;
import io.quarkus.test.junit.QuarkusMock;
import io.quarkus.test.junit.QuarkusTest;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.Optional;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@Disabled // to speed-up demos
@QuarkusTest
public class BurgerMakerControllerTest {

    @Inject
    private BurgerMakerService burgerMakerService;

    @Test
    public void shouldReturnBurger() {

        Burger burger = new Burger("1", "burger", Arrays.asList(new Ingredient("1", "bun")));

        QuarkusMock.installMockForInstance(new BurgerMakerService() {
            @Override
            public Optional<Burger> deliver() {
                return Optional.of(burger);
            }
        }, burgerMakerService);

        given()
                .when().delete("/api/burger/top")
                .then()
                .statusCode(200)
                .body(
                        "name", is("burger"),
                        "ingredients.size()", is(1),
                        "ingredients[0].name", is("bun")
                );
    }

    @Test
    public void shouldReturnNoBurger() {

        QuarkusMock.installMockForInstance(new BurgerMakerService() {
            @Override
            public Optional<Burger> deliver() {
                return Optional.empty();
            }
        }, burgerMakerService);

        given()
                .when().delete("/api/burger/top")
                .then()
                .statusCode(HttpStatus.SC_SERVICE_UNAVAILABLE);
    }

}
