/*
 * Copyright (C) 2021 Orange & contributors
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 *
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */
package com.orange.fastronomy.maker.burger.web;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import lombok.Value;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.util.List;

@Schema(description = "Paginated elements response")
@Value
public class PaginatedResponse<T> {
    @Schema(description = "Total number of elements")
    private final long totalElements;

    @Schema(description = "Total number of pages")
    private final long totalPages;

    @Schema(description = "Number of elements per pages")
    private final long perPage;

    @Schema(description = "Current response page")
    private final long page;

    @Schema(description = "Current page elements")
    private final List<T> elements;

    public PaginatedResponse(PanacheQuery<T> query) {
        this.totalElements = query.count();
        this.totalPages = query.pageCount();
        this.perPage = query.page().size;
        this.page = query.page().index;
        this.elements = query.list();
    }
}
