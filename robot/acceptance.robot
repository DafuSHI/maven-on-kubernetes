*** Settings ***
Library               Collections
Library               RequestsLibrary

*** Test Cases ***
Order Burger
    Create Session    burger            ${BASE_URL}
    ${resp}=          Delete request    burger     /api/burger/top
    Request Should Be Successful        ${resp}
    Dictionary Should Contain Key       ${resp.json()}                               id
    Should Be Equal                     ${resp.json()['name']}                       burger
    Dictionary Should Contain Key       ${resp.json()}                               ingredients
    Length Should Be                    ${resp.json()['ingredients']}                9
    Should Be Equal                     ${resp.json()['ingredients'][0]['name']}     bun
    Should Be Equal                     ${resp.json()['ingredients'][1]['name']}     ketchup
    Should Be Equal                     ${resp.json()['ingredients'][2]['name']}     mayo
    Should Be Equal                     ${resp.json()['ingredients'][3]['name']}     tomato
    Should Be Equal                     ${resp.json()['ingredients'][4]['name']}     salad
    Should Be Equal                     ${resp.json()['ingredients'][5]['name']}     bacon
    Should Be Equal                     ${resp.json()['ingredients'][6]['name']}     cheddar
    Should Be Equal                     ${resp.json()['ingredients'][7]['name']}     steak
    Should Be Equal                     ${resp.json()['ingredients'][8]['name']}     cheddar

List Purchases
    Create Session    burger            ${BASE_URL}
    ${resp}=          Get request       burger     /api/burger/purchases
    Request Should Be Successful        ${resp}
    Dictionary Should Contain Key       ${resp.json()}     elements
