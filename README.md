# Maven on kubernetes project sample

This project sample shows the usage of _to be continuous_ templates:

* Maven
* Docker
* Kubernetes ([Flexible Engine - Cloud Container Engine](https://cloud.orange-business.com/en/offers/infrastructure-iaas/public-cloud/features/cloud-container-engine/))
* Cypress
* Postman
* Robot Framework

The project exposes a basic JSON/Rest API, testable with Swagger UI.

It is a Quarkus application, and uses a MySQL database.

## Maven template features

This project uses the following features from the Maven template:

* Overrides the default Maven docker image by declaring the `$MAVEN_IMAGE` variable in the `.gitlab-ci.yml` file,
* Defines the `SONAR_URL` (enables SonarQube analysis),
* Overrides the `SONAR_BASE_ARGS` with organization & projectKey from [sonarcloud.io](https://sonarcloud.io/),
* Defines :lock: `$SONAR_AUTH_TOKEN` as secret CI/CD variable.

The Maven template also implements [unit tests report](https://docs.gitlab.com/ee/ci/unit_test_reports.html) and
[code coverage](https://docs.gitlab.com/ee/ci/pipelines/settings.html#test-coverage-parsing) integration in GitLab.

## Docker template features

This project builds and _pushes_ a Docker image embedding the built Maven application.
For this, a Docker registry is required.
As a matter of fact there are two options (this project implements the first one).

### Option 1: use the GitLab registry

This is the easiest as the Docker template is preconfigured to work this way.

Extra requirements:

1. make sure the Kubernetes cluster has (network) access to the GitLab registry (true in our case through the internet),
2. create a Kubernetes secret with GitLab registry credentials to `docker pull` images:
    ```bash
    kubectl create secret docker-registry gitlab-registry-credentials --docker-server=registry.gitlab.com --docker-username=token --docker-password=$SOME_GITLAB_TOKEN
    ```
3. use those credentials when using images from GitLab, with the `imagePullSecrets` field.

### Option 2: use the Flexible Engine registry

This project could easily use the Flexible Engine's Docker registry (`registry.eu-west-0.prod-cloud-ocb.orange-business.com`).
For this, the following would be required:

1. make sure the GitLab runners have (network) access to the external registry,
2. Override default `$DOCKER_SNAPSHOT_IMAGE` and `$DOCKER_RELEASE_IMAGE` complying to specific [Flexible Engine's policy](https://docs.prod-cloud-ocb.orange-business.com/usermanual/swr/swr_01_0011.html):
    ```yaml
    DOCKER_SNAPSHOT_IMAGE: registry.eu-west-0.prod-cloud-ocb.orange-business.com/to-be-continuous/burger-maker/snapshot:$CI_COMMIT_REF_SLUG
    DOCKER_RELEASE_IMAGE: registry.eu-west-0.prod-cloud-ocb.orange-business.com/to-be-continuous/burger-maker:$CI_COMMIT_REF_NAME
    ```
3. Define :lock: `$DOCKER_REGISTRY_USER` and :lock: `$DOCKER_REGISTRY_PASSWORD` as secret project variables, obtained according [Flexible Engine documentation](https://docs.prod-cloud-ocb.orange-business.com/usermanual/swr/swr_01_1000.html).

## Kubernetes template features

This project uses the following features from the Kubernetes template:

* Overrides the default `kubectl` version to match CCE version by declaring `$K8S_KUBECTL_IMAGE` in the `.gitlab-ci.yml` file,
* Defines mandatory `$K8S_URL`,
* Defines :lock: `$K8S_CA_CERT` and :lock: `$K8S_TOKEN` (related to a generated [CI/CD service account](https://www.auroria.io/kubernetes-ci-cd-service-account-setup/)) as secret variables,
* Enables review environments by declaring the `$K8S_REVIEW_SPACE` in the project variables,
* Enables staging environment by declaring the `$K8S_STAGING_SPACE` in the project variables,
* Enables production environment by declaring the `$K8S_PROD_SPACE` in the project variables.

The Kubernetes template also implements [environments integration](https://docs.gitlab.com/ee/ci/environments/) in GitLab:

* deployment environment integrated in merge requests,
* review environment cleanup support (manually or when the related feature branch is deleted).

### implementation details

In order to perform Kubernetes deployments, this project implements:

* `k8s-pre-apply.sh` hook script: creates a MySQL database if does not exist,
* `deployment.yml` template: instantiates all required Kubernetes objects,
* `k8s-readiness-check.sh` hook script: waits and checks for the application to respond on `/health/ready` endpoint right after deployment

All those scripts and descriptors make use of variables dynamically evaluated and exposed by the Kubernetes template:

* `${appname}`: the application target name to use in this environment (ex: `maven-on-kubernetes-review-fix-bug-12`)
* `${docker_image}`: the docker image that was just built in the upstream pipeline and that is being deployed
* `${stage}`: the CI job stage (equals `$CI_JOB_STAGE`)
* `${hostname}`: the environment hostname, extracted from `$CI_ENVIRONMENT_URL` (declared as [`environment:url`](https://docs.gitlab.com/ee/ci/yaml/#environmenturl) in the `.gitlab-ci.yml` file)

## Acceptance tests

This project also implements (basic) acceptance tests based on Cypress, Postman and Robot Framework.

All of them implement automatic server url detection: whichever pipeline is executed, those templates automatically
retrieve the latest deployed server (review or staging) and perform tests on it.
