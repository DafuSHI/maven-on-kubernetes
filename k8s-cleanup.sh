#!/bin/sh

set -e

echo "[burger-maker] cleanup script: delete all objects with label app=$appname"
kubectl delete all,pvc,secret,ingress -l "app=$appname"
